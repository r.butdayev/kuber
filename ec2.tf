provider "aws" {
    region = "us-east-1"
  }

 
resource "aws_vpc" "main" {
    cidr_block       = var.main_vpc_cidr
    instance_tenancy = "default"
    enable_dns_support = true
    enable_dns_hostnames = true

     tags = {
         Name = "main_vpc"
            }
        }


 resource "aws_subnet" "consub" {
   vpc_id     = aws_vpc.main.id
   cidr_block = "10.0.10.0/24"
   availability_zone = var.availability_zone1


  tags  =  {
    Name = "controller-subnet"
    }
 }
 

 resource "aws_internet_gateway" "main-igw" {
   vpc_id = aws_vpc.main.id

  tags = {
   Name = "main-igw"
   }
   }


resource "aws_eip" "nat" {
}

resource "aws_nat_gateway" "main-natgw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.consub.id

  tags = {
    Name = "main-nat"
  }
}

resource "aws_route_table" "main-public-rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }

  tags = {
    Name = "main-public-rt"
  }
}

resource "aws_route_table" "main-private-rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.main-natgw.id
  }

  tags  = {
    Name = "main-private-rt"
  }
}

 
resource "aws_route_table_association" "private-assoc-1" {
  subnet_id      = aws_subnet.consub.id
  route_table_id = aws_route_table.main-private-rt.id
}
 
 

resource "aws_instance" "controller-k8s" {

  ami                         = "ami-0be2609ba883822ec"
  key_name                    = "devops"
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.controller-sg.id]
  associate_public_ip_address = true
  
  subnet_id = aws_subnet.consub.id
  
}

resource "aws_security_group" "controller-sg" {
  name   = "controller-security-group"

   vpc_id = aws_vpc.main.id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0 
    to_port     = 0 
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#resource "aws_key_pair" "bastion_key" {
 #  key_name   = "your_key_name"
  # public_key = "ssh-rsa AAA
#}

output "K8s Controller Public IP" {
  value = aws_instance.controller-k8s.public_ip
}
 

